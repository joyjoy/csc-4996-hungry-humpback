﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{
	public float speed;

	void Update()
	{
		if(tag == "FoodFish")
		{
			rigidbody.velocity = transform.right * speed;
		}
		else if(tag == "EnemyFish")
		{
			rigidbody.velocity = transform.forward * speed;
		}
	}
}
