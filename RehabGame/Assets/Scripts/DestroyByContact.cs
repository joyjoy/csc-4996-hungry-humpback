﻿using UnityEngine;
using System.Collections;
//Used by Main - Player
public class DestroyByContact : MonoBehaviour
{
	public GameObject foodCollision;
	public GameObject enemyCollision;
	public int foodScoreValue;
	private GameController gameController;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if(gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if(gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script in DestroyByContact");
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(tag == "Player")
		{
			if(other.tag == "FoodFish")
			{
				Instantiate(foodCollision, other.transform.position, Quaternion.identity);
				Destroy(other.gameObject);
				gameController.AddScore(foodScoreValue);
			}
			else if(other.tag == "EnemyFish")
			{
				Instantiate(enemyCollision, other.transform.position, Quaternion.identity);
				Destroy(other.gameObject);
				gameController.TakeLife();
			}
		}
	}
}
