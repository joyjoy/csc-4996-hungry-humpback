﻿using UnityEngine;
using System.Collections;

//Is used by Main - GameController

public class GameController : MonoBehaviour
{
	public int level = 0;
    //Maybe structures or objects to hold different values would be good?
	//For Start Screen (aka Level 0)
	public GUIText titleText;
	public GUIText subtitleText;
	public GUIText nextLevelText;
	public GUIText configurationText;
	private bool config;
	public GUIText aboutText;
	private bool about;
	public GUIText escapeText;
	private bool escape;
	public bool setPosition;
	public bool reverse;
	//For Levels
	public GameObject food;
	public GameObject enemy;
	public Vector3 foodSpawnValues;
	public Vector3 enemySpawnValues;
	public int foodCount;
	public int enemyCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	private int score;
	public int scoreLimit;
	private int lives;
	public int startingLives;
	public GUIText scoreText;
	public GUIText livesText;
	public GUIText restartText;
	public GUIText gameOverText;
	private bool restart;
	private bool gameOver;
	private bool levelBeat;
	public int gameLength;
	private int timeLeft;

	void Start()
	{
		LoadStartScreen ();
		//Config Settings
		setPosition = false;
		reverse = false;

		gameOver = false;
		restart = false;
		gameOverText.text = "";
		restartText.text = "";
		scoreText.text = "";
		livesText.text = "";
		escapeText.text = "";
	}

	//This handles all the logic for switching between screen views, and also for restarting a level
    //FIXME:  This can be cleaned up somewhat
	void Update()
	{
		if(level == 0)
		{
			if(!about && !config)
			{
				if(Input.GetKeyDown (KeyCode.L))
				{
					UnloadStartScreen();
					LoadLevel1();
				}
				else if(Input.GetKeyDown (KeyCode.A))
				{
					UnloadStartScreen();
					about = true;
					LoadAboutScreen();
				}
				else if(Input.GetKeyDown (KeyCode.C))
				{
					UnloadStartScreen();
					config = true;
					LoadConfigScreen();
				}
			}
			else
			{
				if(Input.GetKeyDown (KeyCode.Escape))
				{
					if(about)
					{
						UnloadAboutScreen();
					}
					else
					{
						UnloadConfigScreen();
					}
					LoadStartScreen();
				}
				else if(Input.GetKeyDown (KeyCode.R))
				{
					if(reverse)
					{
						reverse = false;
					}
					else
					{
						reverse = true;
					}
				}
			}
		}
		else if(restart)
		{
            //FIXME: this can be abstracted to make it one call.
			if(Input.GetKeyDown (KeyCode.Escape))
			{
				if(level==1)
				{
					UnloadLevel1();
				}
				else if(level==2)
				{
					UnloadLevel2();
				}
				else if(level==3)
				{
					UnloadLevel3();
				}
				else if(level==4)
				{
					UnloadLevel4();
				}
				else if(level==5)
				{
					UnloadLevel5();
				}
				else
				{
					Debug.Log("Our level wasn't a possible level, caught error in return to start switch statement in Update function in GameController.cs");
					Application.LoadLevel(Application.loadedLevel);
				}
/*				switch(level)
				{
				case 1:
					UnloadLevel1();
					break;
				case 2:
					UnloadLevel2();
					break;
				case 3:
					UnloadLevel3();
					break;
				case 4:
					UnloadLevel4();
					break;
				case 5:
					UnloadLevel5();
					break;
				default:
					Debug.Log("Our level wasn't a possible level, caught error in return to start switch statement in Update function in GameController.cs");
					Application.LoadLevel(Application.loadedLevel);
				}
*/				LoadStartScreen();
			}
			else if(Input.GetKeyDown (KeyCode.R) && !levelBeat)
			{
				if(level==1)
				{
					UnloadLevel1 ();
					LoadLevel1 ();
				}
				else if(level==2)
				{
					UnloadLevel2 ();
					LoadLevel2();
				}
				else if(level==3)
				{
					UnloadLevel3 ();
					LoadLevel3();
				}
				else if(level==4)
				{
					UnloadLevel4 ();
					LoadLevel4();
				}
				else if(level==5)
				{
					UnloadLevel5 ();
					LoadLevel5 ();
				}
				else
				{
					Debug.Log("Our level wasn't a possible level, caught error in level increment switch statement in Update function in GameController.cs");
					Application.LoadLevel(Application.loadedLevel);
				}
				/*				switch(level)
				{
				case 1:
					UnloadLevel1();
					LoadLevel1();
					break;
				case 2:
					UnloadLevel2();
					LoadLevel2();
					break;
				case 3:
					UnloadLevel3();
					LoadLevel3();
					break;
				case 4:
					UnloadLevel4();
					LoadLevel4();
					break;
				case 5:
					UnloadLevel5();
					LoadLevel5();
					break;
				default:
					Debug.Log("Our level wasn't a possible level, caught error in level increment switch statement in Update function in GameController.cs");
					Application.LoadLevel(Application.loadedLevel);
				}
*/
			}
			else if(Input.GetKeyDown (KeyCode.L) && levelBeat)
			{
				if(level==1)
				{
					UnloadLevel1 ();
					LoadLevel2 ();
				}
				else if(level==2)
				{
					UnloadLevel2 ();
					LoadLevel3();
				}
				else if(level==3)
				{
					UnloadLevel3 ();
					LoadLevel4();
				}
				else if(level==4)
				{
					UnloadLevel4 ();
					LoadLevel5();
				}
				else if(level==5)
				{
					UnloadLevel5 ();
					LoadStartScreen ();
				}
				else
				{
					Debug.Log("Our level wasn't a possible level, caught error in level increment switch statement in Update function in GameController.cs");
					Application.LoadLevel(Application.loadedLevel);
				}
/*				switch(level)
				{
				case 1:
					UnloadLevel1();
					LoadLevel2();
					break;
				case 2:
					UnloadLevel2();
					LoadLevel3();
					break;
				case 3:
					UnloadLevel3();
					LoadLevel4();
					break;
				case 4:
					UnloadLevel4();
					LoadLevel5();
					break;
				case 5:
					UnloadLevel5();
					LoadStartScreen();
					break;
				default:
					Debug.Log("Our level wasn't a possible level, caught error in level increment switch statement in Update function in GameController.cs");
					Application.LoadLevel(Application.loadedLevel);
				}
*/			}
		}
	}

	void LoadStartScreen()
	{
		level = 0;
		about = false;
		config = false;
		titleText.text = "Captain Crunch";
		subtitleText.text = "Rehabilitation Arcade Game";
		nextLevelText.text = "Press 'L' to begin the first Level";
		configurationText.text = "Press 'C' to visit the Configuration page";
		aboutText.text = "Press 'A' to learn more About this project";
	}

	void UnloadStartScreen()
	{
		titleText.text = "";
		subtitleText.text = "";
		nextLevelText.text = "";
		configurationText.text = "";
		aboutText.text = "";
	}

	void LoadAboutScreen()
	{
		aboutText.text = "About\n\n" +
			"The purpose of this game is to help improve the mobility\n" +
			"of those suffering from brain trauma or stroke.\n" +
			"It is meant to be a fun and interactive way or patients to\n" +
			"engage themselves and improve mobility and everyday functionality.\n" +
			"A special Thank You to Dr. G. Conti and Dr. R. Erlandson for\n" +
			"all their hard wark, contribution, and most importantly their\n" +
			"idea for this project.\n\nAlso a big shout out to Team Captain Crunch, makers " +
			"of the game.\n(Shamsad, Lizna, Michael, Rafael)";
		escapeText.text = "Press 'Esc' to return to main menu";
	}

	void UnloadAboutScreen()
	{
		aboutText.text = "";
		escapeText.text = "";
	}

	void LoadConfigScreen()
	{
		setPosition = true;
		livesText.text = "If you'd like to reverse directions, press 'R'";
		subtitleText.text = "Choose where you'd like your character to be,\nhorizontally. Use the left and right arrow keys";
		escapeText.text = "Press 'Esc' to return to main menu";
	}

	void UnloadConfigScreen()
	{
		setPosition = false;
		livesText.text = "";
		subtitleText.text = "";
		escapeText.text = "";
	}

	void LoadLevel1()
	{
		level = 1;
		levelBeat = false;
		score = 0;
		UpdateScore();
		StartCoroutine(SpawnFood());
	}

	void UnloadLevel1()
	{
		scoreText.text = "";
		escapeText.text = "";
		nextLevelText.text = "";
		restartText.text = "";
		restart = false;
		gameOverText.text = "";
		gameOver = false;
	}

	void LoadLevel2()
	{
		level = 2;
		levelBeat = false;
		score = 0;
		UpdateScore ();
		StartCoroutine(SpawnFood());
		StartCoroutine(CountDown());
	}
	void UnloadLevel2()
	{
		scoreText.text = "";
		escapeText.text = "";
		nextLevelText.text = "";
		restartText.text = "";
		restart = false;
		gameOverText.text = "";
		gameOver = false;
	}
	void LoadLevel3()
	{
		level = 3;
		levelBeat = false;
		score = 0;
		UpdateScore ();
		lives = startingLives;
		UpdateLives ();
		StartCoroutine(SpawnEnemy());
	}
	void UnloadLevel3()
	{
		scoreText.text = "";
		livesText.text = "";
		escapeText.text = "";
		nextLevelText.text = "";
		restartText.text = "";
		restart = false;
		gameOverText.text = "";
		gameOver = false;
	}
	void LoadLevel4()
	{
		level = 4;
		levelBeat = false;
		score = 0;
		UpdateScore ();
		lives = startingLives;
		UpdateLives ();
		StartCoroutine(SpawnFood());
		StartCoroutine(SpawnEnemy());
	}
	void UnloadLevel4()
	{
		scoreText.text = "";
		livesText.text = "";
		escapeText.text = "";
		nextLevelText.text = "";
		restartText.text = "";
		restart = false;
		gameOverText.text = "";
		gameOver = false;
	}
	void LoadLevel5()
	{
		level = 5;
		restart = true;
		levelBeat = true;
		aboutText.text = "You beat all the levels we have right now. Good Job!";
		escapeText.text = "Press 'Esc' to return to main menu";
	}
	void UnloadLevel5()
	{
		restart = false;
		aboutText.text = "";
		escapeText.text = "";
	}

	IEnumerator SpawnFood()
	{
		yield return new WaitForSeconds(startWait);
		while(true)
		{
			for(int i=0;i<foodCount;i++)
			{
				Vector3 spawnPosition = new Vector3 (foodSpawnValues.x, foodSpawnValues.y, Random.Range(-foodSpawnValues.z, foodSpawnValues.z));
				Instantiate (food, spawnPosition, Quaternion.identity);
				yield return new WaitForSeconds(spawnWait);
			}
			yield return new WaitForSeconds(waveWait);
			if(gameOver)
			{
				escapeText.text = "Press 'Esc' to return to main menu";
				if(levelBeat)
				{
					nextLevelText.text = "Press 'L' for next Level";
				}
				else
				{
					restartText.text = "Press 'R' for Restart";
				}
				restart = true;
				break;
			}
		}
	}

	IEnumerator SpawnEnemy()
	{
		yield return new WaitForSeconds(startWait);
		while(true)
		{
			for(int i=0;i<enemyCount;i++)
			{
				Vector3 spawnPosition = new Vector3 (enemySpawnValues.x, enemySpawnValues.y, Random.Range(-enemySpawnValues.z, enemySpawnValues.z));
				Quaternion spawnRotation = Quaternion.identity;
				if(reverse)
				{
					spawnRotation.eulerAngles = new Vector3(0,270,180);
				}
				else
				{
					spawnRotation.eulerAngles = new Vector3(0,270,0);
				}
				Instantiate (enemy, spawnPosition, spawnRotation);
				yield return new WaitForSeconds(spawnWait);
			}
			yield return new WaitForSeconds(waveWait*2);
			if(gameOver)
			{
				escapeText.text = "Press 'Esc' to return to main menu";
				if(levelBeat)
				{
					nextLevelText.text = "Press 'L' for next Level";
				}
				else
				{
					restartText.text = "Press 'R' for Restart";
				}
				restart = true;
				break;
			}
		}
	}

	IEnumerator CountDown()
	{
		timeLeft = gameLength;
		UpdateTimeLeft();
		yield return new WaitForSeconds(startWait);
		while(timeLeft>0)
		{
			yield return new WaitForSeconds (1);
			timeLeft--;
			UpdateTimeLeft();
			if(gameOver)
			{
				break;
			}
		}
		if(!gameOver)
		{
			GameOver();
		}
	}

	public void AddScore(int newScoreValue)
	{
		if(!gameOver)
		{
			score += newScoreValue;
			UpdateScore();
		}
	}

	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
		if(score>=scoreLimit)
		{
			levelBeat = true;
			GameOver();
		}
	}

	public void TakeLife()
	{
		lives--;
		UpdateLives();
	}

	void UpdateLives()
	{
		livesText.text = "Lives: " + lives;
		if(lives<=0)
		{
			GameOver();
		}
	}

	void UpdateTimeLeft()
	{
		restartText.text = "Time Left: " + timeLeft;
	}

	public void GameOver()
	{
		gameOver = true;
		if(levelBeat)
		{
			gameOverText.text = "You Won!";
		}
		else
		{
			gameOverText.text = "Getting Better.\nTry Again!";
		}
	}
}
