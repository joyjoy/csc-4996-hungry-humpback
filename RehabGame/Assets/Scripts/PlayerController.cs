﻿//Used by Main - PlayerController
using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
	public float speed; //THIS IS ONLY NEEDED FOR MOVING WITH KEYS, A SQUEEZE INPUT WON'T NEED IT
	public Boundary boundary;
	private GameController gameController;
	private bool setPosition = false;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if(gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if(gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script in PlayerController");
		}
	}

	void FixedUpdate ()
	{
		setPosition = gameController.setPosition;
		//THIS SHOULD BE A DIFFERENT INPUT THAN THE KEYBOARD, THEN THE NEXT LINE WILL CHANGE WHAT INPUT MOVEVERTICAL IS EQUAL TO
		float moveVertical = Input.GetAxis("Vertical");
		float movementTemp = 0.0f;
		float positionTemp = rigidbody.position.x;
		if(setPosition)
		{
			//float moveVertical = Input.GetAxis("Vertical");
			float moveHorizontal = Input.GetAxis ("Horizontal");
			if(!gameController.reverse)
			{
				movementTemp = moveHorizontal;
			}
			else
			{
				movementTemp = -moveHorizontal;
			}
			positionTemp = Mathf.Clamp (rigidbody.position.x, boundary.xMin, boundary.xMax);
		}
		Vector3 movement = new Vector3 (movementTemp, 0.0f, moveVertical);
		rigidbody.velocity = movement * speed;
		rigidbody.position = new Vector3 (positionTemp, 0.0f, Mathf.Clamp (rigidbody.position.z, boundary.zMin, boundary.zMax));
	}
}
