﻿using UnityEngine;
using System.Collections;

//Used by Main - Boundary
public class DestroyByBoundary : MonoBehaviour
{
	public int enemyScoreValue;
	private GameController gameController;
	
	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if(gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if(gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script in DestroyByBoundary");
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag != "Player")
		{
			if(other.tag == "EnemyFish")
			{
				gameController.AddScore(enemyScoreValue);
			}
			Destroy (other.gameObject);
		}
	}
}
