﻿using UnityEngine;
using System.Collections;

//This script is used by the scene Main-Background and Main-Camera
public class Reverse : MonoBehaviour
{

	private bool reverse;
	private GameController gameController;
	
	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if(gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if(gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script in DestroyByContact");
		}
		
		reverse = false;
	}

	void Update ()
	{
        //FIXME:  Although it is trivial, it would be an improvement to test this condition at the beginning of a scene, and 
        //not again until the next scene.
		if(reverse != gameController.reverse)
		{
			reverse = gameController.reverse;
			ReverseItem();
		}
	}
	
	void ReverseItem()
	{
		transform.position = new Vector3 (transform.position.x, transform.position.y * -1, transform.position.z);
		Quaternion tempRotation = Quaternion.identity;
		if(reverse)
		{
			tempRotation.eulerAngles = new Vector3 (transform.eulerAngles.x*-1, 180.0f, transform.eulerAngles.z);
		}
		else
		{
			tempRotation.eulerAngles = new Vector3 (transform.eulerAngles.x*-1, 0.0f, transform.eulerAngles.z);
		}
		transform.rotation = tempRotation;
	}
}
